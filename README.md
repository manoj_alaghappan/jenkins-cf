# Project Title
A cloud formation template that sets up a Jenkins installation and autoscaling group for the agents with docker.

## Authors
 **Manoj Balaji Alaghappan**  - https://gitlab.com/manoj_alaghappan

## Prerequisites
AWS account with permission to run Cloudformation(CF) and delpoy the instances created as part of the CF template

## Deployment
Launch the CF template via AWS Console or CLI
**Parameters required**
- ProjectName: Name of the Project to be used in Tags (1-8 char long).
- KeyName: The EC2 Key Pair to allow SSH access to the Jenkins Master instance.
- JenkinsMasterAdminPassword: A password for the Jenkins master admin user.

## Versioning
For the versions available, see the [tags on this repository]
(https://gitlab.com/manoj_alaghappan/jenkins-cf/tags).
* Version <1.0>  
    Version Features
      - Delpoys the VPC environment with 2 public and 2 private subnet,public
       and private Route table, Internet gateway.
      Note: Private subnets ae created for future updates to incorporate
       security best practices.
      - Deploy and configure Jenkins Master InstanceType.
      - Create an Autoscaling Launch configuration for Jenkins Agent Docker
       container. And Create a Auto scaling group.
      - Outputs the Jenkins master URL   
